(use-package ox-latex
  :defer t
  :init
  (defun elementaryx/remove-from-org-latex-default-packages-alist (package)
    "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-default-packages-alist'. This
issues useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
    (let ((output '()))
      (dolist (pkg org-latex-default-packages-alist)
	(unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
      (setq org-latex-default-packages-alist output)))
  (defun elementaryx/remove-from-org-latex-packages-alist (package)
    "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-packages-alist'. This is
useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
    (let ((output '()))
      (dolist (pkg org-latex-packages-alist)
	(unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
      (setq org-latex-packages-alist output)))
  :config
  (add-to-list 'org-latex-classes
               '("compas2021"
                 "\\documentclass{compas2021}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
  (add-to-list 'org-latex-classes
               '("IEEEtran"
		 "\\documentclass{IEEEtran}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
  (add-to-list 'org-latex-classes
               '("llncs"
                 "\\documentclass{llncs}
\\usepackage{hyperref, wrapfig}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
  (add-to-list 'org-latex-classes
               '("siamart220329"
                 "\\documentclass{siamart220329}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
  (add-to-list 'org-latex-classes
               '("acmart"
                 "\\documentclass{acmart}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(provide 'elementaryx-ox-latex-classes)
